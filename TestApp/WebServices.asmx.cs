﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;

namespace TestApp
{
    /// <summary>
    /// Summary description for WebServices
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class WebServices : System.Web.Services.WebService
    {

        [WebMethod]

        public string HelloWorld()
        {
            
            return "Hello World";
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public string TokenCheck(string Token)
        {
           
            
            string tk = "g1Teed4WlbK+QmKchjtaMd4U1XaXiA6BGbWm9Hilc4cTWXkXtKxbt9ZIBGRkvF90EklNQX267ZhIUvCAPpa+5L3wIdvxOo8OtBmVcPNOVBiy/2wCIamVrSErq0C0vgNYAkxC+nwNVF5jqUXt6AkqzzesCJkbionixfB5PkVQ1BE=";
            //string tk = "dferwgfe";
            if (tk.Equals(Token))
            {
                return "Hello";
            }
            else
            {
                return "fucku";
            }
            
        }
        
    }
}
